<?php

namespace Drupal\template_suggestion;

/**
 * Global types for template suggestion module.
 *
 * @category PHP
 */
abstract class TemplateSuggestionTemplateTypes {
  const TEMPLATE_SUGGESTION_ENTITY_TYPE = 'node';
  const TEMPLATE_SUGGESTION_FIELD_NAME = 'field_template_suggestion';
  const TEMPLATE_SUGGESTION_BUNDLE = 'page';

}
