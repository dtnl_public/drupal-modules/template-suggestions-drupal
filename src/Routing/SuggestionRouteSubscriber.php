<?php

namespace Drupal\template_suggestion\Routing;

use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\template_suggestion\Manager\SuggestionManager;
use Symfony\Component\Routing\Route;

/**
 * Listens to the dynamic route events.
 */
class SuggestionRouteSubscriber {


  /**
   * @var \Drupal\template_suggestion\Manager\SuggestionManager
   */
  private $suggestionManager;

  /**
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   *
   */
  public function __construct(
    SuggestionManager $suggestionManager,
    LanguageManagerInterface $languageManager
  ) {
    $this->suggestionManager = $suggestionManager;
    $this->languageManager = $languageManager;
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $routes = [];

    $nodes = $this->suggestionManager->getSuggestionNodes();
    $languages = $this->languageManager->getLanguages();

    /**
     * @var \Drupal\node\NodeInterface $nodeInterface
     */
    foreach ($nodes as $node) {
      foreach ($languages as $language) {
        $langcode = $language->getId();
        if (!$node->hasTranslation($langcode)) {
          continue;
        }
        $node = $node->getTranslation($langcode);

        // Add route for the default language.
        $templateSuggestion = $node->get('field_template_suggestion')->value;
        $routes["template_suggestion.$templateSuggestion.$langcode"] = new Route(
          $node->toUrl()->toString()
        );
        // If default, add a default route.
        if ($language->isDefault()) {
          $routes["template_suggestion.$templateSuggestion.default"] = new Route(
            $node->toUrl()->toString()
          );
        }
      }
    }

    return $routes;
  }

}
