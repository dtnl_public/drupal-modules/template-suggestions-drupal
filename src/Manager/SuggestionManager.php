<?php

namespace Drupal\template_suggestion\Manager;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Manages template suggestion fielded Nodes.
 */
class SuggestionManager {

  const ENTITY_TYPE = 'node';

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityManager;

  /**
   * The storage interface.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $storage;

  /**
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   * @param \Drupal\Core\Session\AccountProxyInterface $accountProxy
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(
    EntityTypeManagerInterface $entityManager
  ) {
    $this->entityManager = $entityManager;
    $this->storage = $entityManager->getStorage(self::ENTITY_TYPE);
  }

  /**
   *
   */
  public function getSuggestionNodes() {
    return $this->storage->loadMultiple(
      $this->storage->getQuery()
        ->condition('field_template_suggestion', ['null', '_none'], 'NOT IN')
        ->execute()
    );
  }

}
