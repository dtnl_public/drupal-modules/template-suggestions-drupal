<?php

namespace Drupal\template_suggestion\Twig;

use Drupal\Core\Routing\RouteProviderInterface;

/**
 *
 */
class SuggestionTwigExtension extends \Twig_Extension {

  /**
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  private $routeProvider;

  /**
   *
   */
  public function __construct(RouteProviderInterface $routeProvider) {
    $this->routeProvider = $routeProvider;
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'template_suggestion.twig_extension';
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('suggestion_route', [$this, 'getSuggestionRoute']),
    ];
  }

  /**
   * Get the route of a template suggestion without symfony throwing
   * a route not found exception.
   *
   * @param string $route
   *   name of the route.
   *
   * @return string
   *   the path or an anchor if not existing.
   */
  public function getSuggestionRoute($routeName) {
    try {
      $route = $this->routeProvider->getRouteByName($routeName);
      return $route->getPath();
    }
    catch (\Exception $exception) {
      return '#route-not-found';
    }
  }

}
