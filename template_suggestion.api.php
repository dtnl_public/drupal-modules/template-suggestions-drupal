<?php

/**
 * @file
 * API definitions for the template suggestion module.
 */

/**
 * Get available template suggestion options.
 */
function hook_template_suggestion_suggestion_options(&$options = []) {
  // Where node machine name is the machine name
  // and key is the template suggestion machine name
  // And value is the template suggestion name.
  $options['node_machine_name'] = ['key' => 'value'];
}
